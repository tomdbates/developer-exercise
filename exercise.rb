class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
    # TODO: Implement this method
    words = str.split()
    words.each_with_index { |word, i|
        self.wordCheck(word)
        if(word.length > 4)
          words[i] = self.wordCheck(word)
        end
    }

    # Put it back together, assuming we don't have to reproduce exact white space lengths between words
    return words.join(' ')
  end

  # Checks word for ending (normal, single char) punctuation, preserving any it finds and calls marklarIt if appropriate
  def self.wordCheck(word)
    result = word
    # just need last char so don't bother with slice
    endc = word[word.length - 1]

    if endc.end_with?(".", "?", "!", ":", ";")
      if(word.length - 1 > 4)    # unpunc'd part is > 4
        result = self.marklarIt(word[0..word.length-1]) + endc
      end
    else
      result = self.marklarIt(word)
    end

    return result
  end

  def self.marklarIt(word)
    return (word[0].capitalize == word[0]) ? "Marklar" : "marklar"
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
    # TODO: Implement this method

    # Check for edgy input
    fsum = 0
    if nth < 2
      return fsum
    end

    self.fibonaccis(nth - 1)
    for f in @fibos
        if f%2 == 0
          fsum += f
        end
    end
    return fsum
  end
  
  @fibos = Array.new(2, 1)    # i.e., [1, 1]
  # A simpler fibonacci list generator sans recursion is really all we need
  def self.fibonaccis(nth)
    if nth < 3
      return
    end
    i = @fibos.length;
    while @fibos.length < (nth + 1)
      @fibos.push(@fibos[@fibos.length - 1] + @fibos[@fibos.length - 2])
    end
  end

end
